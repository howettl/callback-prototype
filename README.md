# README #

### What is this repository for? ###

Shows an example of how to implement fragment callbacks which do not require interfaces to be implemented in the base activity, while still being retained between configuration changes.