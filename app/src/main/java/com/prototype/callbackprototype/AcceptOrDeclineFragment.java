package com.prototype.callbackprototype;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lee Howett on 2016-12-09.
 * <p>
 * com.prototype.callbackprototype
 */

public class AcceptOrDeclineFragment extends Fragment {

    public interface Callback {
        void onAccepted();
        void onDeclined();
    }

    private Callback mCallback;

    public static AcceptOrDeclineFragment newInstance(Callback callback) {
        AcceptOrDeclineFragment f = new AcceptOrDeclineFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        f.setTargetFragment((Fragment) callback, 0);
        return f;
    }

    @Override
    public void setTargetFragment(Fragment fragment, int requestCode) {
        if (!(fragment instanceof Callback)) throw new IllegalArgumentException("Fragment used to load AcceptOrDeclineFragment must implement the Callback " +
                "interface.");
        else super.setTargetFragment(fragment, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_accept_or_decline, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getTargetFragment() != null && getTargetFragment() instanceof Callback) {
            mCallback = (Callback) getTargetFragment();
        }
    }

    @OnClick(R.id.accept_button)
    void onAcceptClicked() {
        if (mCallback != null) mCallback.onAccepted();
    }

    @OnClick(R.id.decline_button)
    void onDeclineClicked() {
        if (mCallback != null) mCallback.onDeclined();
    }
}
