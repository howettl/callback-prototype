package com.prototype.callbackprototype;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Lee Howett on 2016-12-09.
 * <p>
 * com.prototype.callbackprototype
 */

public class DisplayStateFragment extends Fragment implements AcceptOrDeclineFragment.Callback {

    private static final String KEY_ACCEPTED = "accepted";

    public static DisplayStateFragment newInstance() {
        DisplayStateFragment f = new DisplayStateFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_ACCEPTED, false);
        f.setArguments(args);
        return f;
    }

    @BindView(R.id.state_label)
    TextView mStateLabel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_display_state, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onAccepted() {
        getArguments().putBoolean(KEY_ACCEPTED, true);
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onDeclined() {
        getArguments().putBoolean(KEY_ACCEPTED, false);
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();

        mStateLabel.setText(getArguments().getBoolean(KEY_ACCEPTED) ? "Accepted": "Declined");
    }

    @OnClick(R.id.change_button)
    void onChangeClicked() {
        ((MainActivity) getActivity()).changeFragment(AcceptOrDeclineFragment.newInstance(this), true, "accept_or_decline");
    }
}
